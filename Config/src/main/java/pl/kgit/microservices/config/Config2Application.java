package pl.kgit.microservices.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class Config2Application {

	public static void main(String[] args) {
		SpringApplication.run(Config2Application.class, args);
	}
}
