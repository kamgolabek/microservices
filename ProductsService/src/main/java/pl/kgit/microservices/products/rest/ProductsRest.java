package pl.kgit.microservices.products.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.kgit.microservices.products.model.Product;
import pl.kgit.microservices.products.repository.ProductsRepository;

@RestController
@RequestMapping("/products")
public class ProductsRest {

	@Autowired
	ProductsRepository repo;
	
	@PostMapping
	public void addProduct(Product p ) {
		repo.addProduct(p);
	}
	
	@GetMapping
	public List<Product> getProducts(){
		return repo.getProducts();
	}
	
	
}
