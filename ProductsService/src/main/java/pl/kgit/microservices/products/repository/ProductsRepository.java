package pl.kgit.microservices.products.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import lombok.Data;
import pl.kgit.microservices.products.model.Product;

@Data
@Repository
public class ProductsRepository {

	private List<Product> products = new ArrayList<>();
	
	
	public void addProduct(Product p) {
		products.add(p);
	}
	
}
