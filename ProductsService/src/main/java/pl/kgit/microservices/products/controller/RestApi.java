package pl.kgit.microservices.products.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
@RefreshScope
public class RestApi {

	@Value("${param.value}")
	String value;
	
	@GetMapping("/")
	@ResponseBody
	public String home() {
		return "Value: " + value;
	}
	
}
