package pl.kgit.microservices.products.model;

import lombok.Data;

@Data
public class Product {
	private String name;
	private String cost;
}
