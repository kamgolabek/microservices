package pl.kgit.microservices.users.restapi;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;


// load balanced instance injected
@FeignClient("ProductsService")
public interface ProductsApi {

	@GetMapping("/products")
	public String getAllProducts();
	
}
