package pl.kgit.microservices.users.restapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductsClientDemoRest {

	@Autowired
	ProductsApi api;
	
	@GetMapping("/")
	public String getAllProducts() {
		return api.getAllProducts();
	}
	
}
